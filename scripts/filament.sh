#!/bin/bash

# === Simple script to simulate a cylinder (filament) in a rectangular box ===

export OMP_NUM_THREADS=8
export I_WILL_NOT_PUBLISH_CRAP=yes
MHD="yes" # no
# define input parameters

unitM="solarm"
unitR="pc"
N_1000="450"
N="$N_1000""000"
cR="0.1000"
cL="1.500"
bLL="2.000"
bLS="0.4000"
contrast="30.00"
mass="37"
cs="3.477"
bzero="113.4"
rotationaxis="180"
perturb="no"
sink="yes"
rhoc_power="14"
rhoc="1.0E-""$rhoc_power"
hac="120"
hacc="$hac""au"
ieos="8"

if [ $MHD = "yes" ]; then
	modelName="N$N_1000""M$mass""rho$rhoc_power""h$hac""B$bzero"
	# write input parameters in modelName
	 
echo "$unitM
$unitR
$N
$cR
$cL
$bLL
$bLS
$contrast
$mass
$cs
$bzero
$rotationaxis
$perturb
$sink
$rhoc
$hacc
$ieos" > "$modelName"".input"
else
	modelName="N$N_1000""M$mass""rho$rhoc_power""h$hac"
	# write input parameters in modelName
	 
echo "$unitM
$unitR
$N
$cR
$cL
$bLL
$bLS
$contrast
$mass
$cs
$perturb
$sink
$rhoc
$hacc
$ieos" > "$modelName"".input"
fi

echo -e "\n Writing input parameters into $modelName"

./phantomsetup $modelName < "$modelName"".input" &> phantomsetup.log
nohup /usr/bin/time -p -o time.txt ./phantom $modelName.in > out.log & 2>&1
