!--------------------------------------------------------------------------!
! The Phantom Smoothed Particle Hydrodynamics code, by Daniel Price et al. !
! Copyright (c) 2007-2018 The Authors (see AUTHORS)                        !
! See LICENCE file for usage and distribution conditions                   !
! http://users.monash.edu.au/~dprice/phantom                               !
!--------------------------------------------------------------------------!
!+
!  MODULE: spherical
!
!  DESCRIPTION:
!   This module sets up spherical particle distributions
!   By default this is done by cropping and stretching cubes
!
!  REFERENCES: None
!
!  OWNER: Daniel Price
!
!  $Id$
!
!  RUNTIME PARAMETERS: None
!
!  DEPENDENCIES: io, physcon, prompting, stretchmap, unifdis
!+
!--------------------------------------------------------------------------
module cylindrical
 use physcon,    only:pi,twopi,fourpi,piontwo
 use unifcyl,    only:set_unifcyl
 use io,         only:fatal,warning
 !
 implicit none
 !
 public  :: set_unifdis_cylinderN
 !
 private
 !
contains
!
!-----------------------------------------------------------------------
!+
!  This subroutine positions particles on a sphere - uniform
!  density by default, or if a function rhofunc(r) or a tabulated
!  array rhotab(:) is passed, with an arbitrary radial density profile rho(r)
!
!  The function is assumed to be a real function with a single argument:
!
!  real function rhofunc(r)
!   real, intent(in) :: r
!
!   rhofunc = 1./r**2
!
!  end function rhofunc
!
!  The table rhotab(:) is assumed to contain density values
!  on equally spaced radial bins between rmin and rmax
!+
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
!+
!  If setting up a uniform sphere, this will iterate to get
!  approximately the desired number of particles
!  Note: Since this is only for a sphere, the call to this is the
!        same as if a sphere were being created by set_unifdis
!+
!-----------------------------------------------------------------------
subroutine set_unifdis_cylinderN(lattice,id,master,xmin,xmax,ymin,ymax,zmin,zmax,psep,&
                    hfact,npart,nps_requested,xyzh,r_cylinder,h_cylinder,v_cylinder,&
                    npart_total)
 use prompting,    only:prompt
 character(len=*), intent(in)    :: lattice
 integer,          intent(in)    :: id,master
 integer,          intent(inout) :: npart,nps_requested
 real,             intent(in)    :: xmin,xmax,ymin,ymax,zmin,zmax,hfact,r_cylinder,v_cylinder
 real,             intent(in)    :: h_cylinder
 real,             intent(out)   :: psep,xyzh(:,:)
 integer(kind=8),  intent(inout) :: npart_total
 integer(kind=8)                 :: npart_local
 integer                         :: nps_lo,nps_hi,npr_lo,npr_hi,test_region,iter
 integer                         :: npin,npmax,npart0,nx,np,dn
 logical                         :: iterate_to_get_nps
 !
 !--Initialise values
 test_region   = 10
 npmax         = size(xyzh(1,:))
 nps_lo        = 0
 nps_hi        = npmax
 npr_lo        = 0
 npr_hi        = npmax
 np            = nps_requested
 dn            = nps_requested/20
 iter          = 0
 npin          = npart
 npart_local   = npart_total
 iterate_to_get_nps = .true.
 !
 print*, ' set_cylinder: Iterating to form cylinder with approx ',nps_requested,' particles'
 !
 !--Perform the iterations
 do while (iterate_to_get_nps .and. iter < 100)
    iter = iter + 1
    nx   = int(np**(1./3.)) - 1           ! subtract 1 because of adjustment due to periodic BCs
    psep = (v_cylinder)**(1./3.)/real(nx) ! particle separation in cylinder
    if (lattice=='closepacked') psep = psep*sqrt(2.)**(1./3.)         ! adjust psep for close-packed lattice
    npart       = npin
    npart_total = npart_local
    call set_unifcyl(lattice,id,master,xmin,xmax,ymin,ymax,zmin,zmax,psep,&
                    hfact,npart,xyzh,rcylmax=r_cylinder,hcyl=h_cylinder,&
                    nptot=npart_total,verbose=.false.,cyl_surround_medium=.false.)
    npart0 = npart - npin
    print*,iter,npart,nps_requested,npart_total,np,npin
!     if (npart0==nps_requested) then
    if (abs(npart0-nps_requested) <=300) then ! smhr: workaround for a problem for random distribution setup
       iterate_to_get_nps = .false.
    elseif (npart0 < nps_requested .and. nps_hi==npmax) then
       ! initialising for the case where npart0 is too small
       nps_lo = npart0
       npr_lo = np
       np     = np + dn
    elseif (npart0 > nps_requested .and. nps_lo==0) then
       ! initialising for the case where npart0 is too large
       nps_hi = npart0
       npr_hi = np
       np     = np - dn
    else
       if (nps_lo==0 .or. nps_hi==npmax) then
          ! finalise the boundaries, and begin testing upwards
          if (nps_lo == 0    ) then
             nps_lo = npart0
             npr_lo = np
          else if (nps_hi == npmax) then
             nps_hi = npart0
             npr_hi = np
          endif
          np = npr_lo + (npr_hi - npr_lo)/3
          test_region = -1
       elseif (test_region == -1) then
          if (npart0 <= nps_lo .or. npart0 > nps_requested) then
             test_region = 1
             np     = npr_lo +  2*(npr_hi - npr_lo)/3
          else
             nps_lo = npart0
             npr_lo = np
             np     = npr_lo + (npr_hi - npr_lo)/3
          endif
       elseif (test_region == 1) then
          if (npart0 >= nps_hi .or. npart0 <= nps_requested ) then
             ! this should be compelete
             if (nps_lo > nps_requested .or. nps_requested > nps_hi) then ! sanity check
                call fatal("set_cylinder","Did not converge to the correct two options for number of particles in the cylinder.")
             endif
             ! always use more particles than requested
             if (nps_requested - nps_lo < nps_hi - nps_requested) then
                write(*,'(a,I8,a,F5.2,a)') " set_sphere: The closest number of particles to the requested number is " &
                    ,nps_lo,", which is ",float(nps_requested-nps_lo)/float(nps_requested)*100.0 &
                    ,"% than less requested."
                write(*,'(a)') " set_sphere: We will not use fewer than the requested number of particles."
             endif
             write(*,'(a,I8,a,F5.2,a)') " set_sphere: Using " &
              , nps_hi," particles, which is ",float(nps_hi - nps_requested)/float(nps_requested)*100.0 &
              ,"% more than requested."
             nps_requested = nps_hi
             np            = npr_hi
          else
             nps_hi = npart0
             npr_hi = np
             np     = npr_lo + 2*(npr_hi - npr_lo)/3
          endif
       else
          call fatal("set_cylinder","iterating to get npart_cylinder.  This option should not be possible")
       endif
    endif
 enddo
 if (iter >= 100) call fatal("set_cylinder","Failed to converge to the correct number of particles in the cylinder")
 write(*,'(a,I10,a)') ' set_cylinder: Iterations complete: added ',npart0,' particles in cylinder'
 !
end subroutine set_unifdis_cylinderN
!--------------------------------------------------------------------------
end module cylindrical
